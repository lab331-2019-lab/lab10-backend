
package se331.lab.rest.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@NoArgsConstructor
@AllArgsConstructor
@Data
@Builder
public class CourseDto {
    Long id;
    String courseId;
    List<StudentDto> students;
    List<LecturerDto> lecturers;
}