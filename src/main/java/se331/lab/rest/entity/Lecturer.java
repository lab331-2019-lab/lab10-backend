package se331.lab.rest.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonManagedReference;
import lombok.*;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

@Entity
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class Lecturer {
    @Id
    String name;
    String surname;
    //@JsonManagedReference
    @OneToMany(mappedBy = "advisor")
    @Builder.Default
    @ToString.Exclude
    @JsonManagedReference
    List<Student> advisees = new ArrayList<>();
    @OneToMany(mappedBy = "lecturer")
    @JsonIgnore
    //@JsonManagedReference
    @Builder.Default
    @EqualsAndHashCode.Exclude
    @ToString.Exclude
    List<Course> courses = new ArrayList<>();
}
