package se331.lab.rest.entity;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonManagedReference;
import lombok.*;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

@Entity
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class Course {
    @Id
    String content;
    String courseId;
    String courseName;
    @ManyToOne
    @JsonBackReference
    Lecturer lecturer;
    @ManyToMany
    @Builder.Default
    @JsonManagedReference

    @EqualsAndHashCode.Exclude
    @ToString.Exclude
    List<Student> students =new ArrayList<>();
}
